import express from "express";
import helmet from "helmet";

import routes from "./routes";

const app = express();

app.use(
  helmet({
    contentSecurityPolicy: {
      directives: {
        "default-src": "'self' *",
        "script-src": "'unsafe-inline' *",
      },
    },
  })
);
app.disable("x-powered-by");

app.use(routes);

export default app;
