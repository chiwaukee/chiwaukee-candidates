import { BrowserRouter, Routes, Route } from "react-router-dom";

import { Menu } from "./Components/Menu";
import { Home } from "./Pages/Home";
import { Candidates } from "./Pages/Candidates";
import { Mission } from "./Pages/Mission";

export function App() {
  return (
    <BrowserRouter>
      <Menu />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/candidates" element={<Candidates />} />
        <Route path="/about" element={<Mission />} />
      </Routes>
    </BrowserRouter>
  );
}
