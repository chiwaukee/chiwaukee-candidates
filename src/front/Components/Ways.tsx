export function Ways() {
  return (
    <section className="section" id="whatwedo">
      <div className="container has-text-white">
        <h2 className="title is-2">What We Can Do</h2>
        <div className="columns is-multiline is-fullwidth">
          <div className="column is-4">
            <div
              className="box has-background-primary has-text-centered has-text-white is-size-4"
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Develop Turnkey & Scalable Web Applications and Sites
            </div>
          </div>
          <div className="column is-4">
            <div
              className="box has-background-primary has-text-centered has-text-white is-size-4"
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Refactor & Reduce Complexity of Existing Codebases
            </div>
          </div>
          <div className="column is-4">
            <div
              className="box has-background-primary has-text-centered has-text-white is-size-4"
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Implement CI / CD Best Practices & Workflows
            </div>
          </div>
          <div className="column is-4">
            <div
              className="box has-background-primary has-text-centered has-text-white is-size-4"
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Improve Development Processes & Methods
            </div>
          </div>
          <div className="column is-4">
            <div
              className="box has-background-primary has-text-centered has-text-white is-size-4"
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Upskill Employees & Drive Engagement
            </div>
          </div>
          <div className="column is-4">
            <div
              className="box has-background-primary has-text-centered has-text-white is-size-4"
              style={{
                height: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              Architect Cloud-based Solutions
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
