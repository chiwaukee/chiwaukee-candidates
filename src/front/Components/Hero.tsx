import { faCopy, faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toast } from "bulma-toast";
import SVG from "react-inlinesvg";

import logo from "../images/chiwaukee-logo.svg";
export function Hero({
  title,
  mission,
  email,
}: {
  title: any;
  mission: string;
  email: string;
}) {
  return (
    <section className="section" id="top">
      <div className="container" style={{ height: "100%" }}>
        <div
          className="columns is-fullwidth is-multiline"
          style={{ height: "100%" }}
        >
          <div
            className="column is-6"
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <div className="content">
              <h1 className="title" style={{ fontSize: "3.5rem" }}>
                {title}
              </h1>

              <h4 className="subtitle is-4" style={{ fontWeight: "normal" }}>
                {email}
              </h4>
              <div style={{ marginTop: "-1.25rem" }} className="is-size-5">
                <a
                  title="Open your email client"
                  href={`mailto:${email}`}
                  className="has-color-primary"
                  style={{
                    display: "inline",
                    height: "100%",
                    cursor: "pointer",
                  }}
                >
                  <FontAwesomeIcon icon={faEnvelope} />
                </a>
                &nbsp;&nbsp;
                <span
                  title="Copy our email to your clipboard"
                  className="has-color-primary"
                  style={{
                    display: "inline",
                    height: "100%",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    navigator.clipboard.writeText(email);
                    toast({
                      duration: 3000,
                      message: "email address copied to clipboard",
                      type: "is-success",
                      dismissible: true,
                    });
                  }}
                >
                  <FontAwesomeIcon icon={faCopy} />
                </span>
              </div>
            </div>
            <div className="content has-text-centered mission">
              <p style={{ fontSize: "1.5rem" }}>{mission}</p>
            </div>
          </div>
          <div className="column is-6 svg-wrapper">
            <SVG src={logo} className="logo-svg" />
          </div>
        </div>
      </div>
    </section>
  );
}
