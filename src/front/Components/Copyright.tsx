export function Copyright() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        padding: "2rem",
      }}
    >
      <p>Copyright © 2022 - Chiwaukee LLC</p>
    </div>
  );
}
