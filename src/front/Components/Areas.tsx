import {
  faAws,
  faDocker,
  faGitlab,
  faGoogle,
  faNodeJs,
  faReact,
} from "@fortawesome/free-brands-svg-icons";
import { faComments } from "@fortawesome/free-regular-svg-icons";
import {
  faCodeCompare,
  faCube,
  faDatabase,
  faForwardFast,
  faHandshakeAngle,
  faMoneyBillTrendUp,
  faServer,
  faCloudBolt,
  faBars,
  faArrowLeft,
  faPuzzlePiece,
  faShieldHalved,
  faBug,
  faMagnifyingGlassDollar,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function Areas({
  title,
  subtitle,
}: {
  title: string;
  subtitle?: string;
}) {
  return (
    <section className="section has-text-white" id="howwedo">
      <div className="container has-text-centered has-text-white">
        <h2 className="title is-2 has-text-left">{title}</h2>
        {subtitle && (
          <h4
            style={{ paddingTop: "1rem", paddingBottom: "1rem" }}
            className="subtitle is-4 has-text-left"
          >
            {subtitle}
          </h4>
        )}
        <div className="columns is-multiline has-text-white">
          <div className="column is-8">
            <div className="has-background-primary area-of-expertise">
              <div
                className="content has-text-centered"
                style={{ width: "100%" }}
              >
                <p className="title has-text-white area-title">
                  Full Stack Development
                </p>
                <div className="is-size-5 area-items">
                  <div className="area-item">
                    <FontAwesomeIcon icon={faNodeJs} />
                    <br />
                    Node JS
                  </div>
                  <div className="area-item">
                    <FontAwesomeIcon icon={faReact} />
                    <br />
                    React
                  </div>
                  <div className="area-item">
                    <FontAwesomeIcon icon={faDocker} />
                    <br />
                    Docker
                  </div>
                </div>
                <div className="is-size-5 area-items">
                  <div className="area-item">
                    <FontAwesomeIcon icon={faServer} />
                    <br />
                    Kubernetes
                  </div>
                  <div className="area-item">
                    <FontAwesomeIcon icon={faAws} />
                    <br />
                    AWS
                  </div>
                  <div className="area-item">
                    <FontAwesomeIcon icon={faGoogle} />
                    <br />
                    Google Cloud
                  </div>
                </div>
                <div className="is-size-5 area-items">
                  <div className="area-item">
                    <FontAwesomeIcon icon={faDatabase} />
                    <br />
                    NoSQL / SQL
                  </div>
                  <div className="area-item">
                    <FontAwesomeIcon icon={faBars} />
                    <br />
                    Queues
                  </div>
                  <div className="area-item">
                    <FontAwesomeIcon icon={faCloudBolt} />
                    <br />
                    Serverless
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-4">
            <div className="has-background-primary area-of-expertise">
              <p className="title has-text-white area-title">Dev / Ops</p>
              <div
                className="content has-text-centered"
                style={{
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <div className="is-size-5 area-items">
                  <div
                    title="Continuous Integration / Continuous Deployment"
                    className="area-item"
                  >
                    <FontAwesomeIcon icon={faCodeCompare} />
                    <br />
                    CI / CD
                  </div>
                  <div
                    title="Software Development LifeCycle"
                    className="area-item"
                  >
                    <FontAwesomeIcon icon={faCube} />
                    <br />
                    SDLC
                  </div>
                  <div
                    title="Static Application Security Testing"
                    className="area-item"
                  >
                    <FontAwesomeIcon icon={faShieldHalved} />
                    <br />
                    SAST
                  </div>
                </div>
                <div className="is-size-5 area-items">
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faGitlab} />
                    <br />
                    GitLab
                  </div>
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faBug} />
                    <br />
                    SonarQube
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-6">
            <div className="has-background-primary area-of-expertise ">
              <p className="title has-text-white area-title">
                Agile Methodology
              </p>
              <div
                className="content has-text-centered"
                style={{
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <div className="is-size-5 area-items">
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faHandshakeAngle} />
                    <br />
                    Scrum
                  </div>
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faMoneyBillTrendUp} />
                    <br />
                    Process Optimization
                  </div>
                </div>
                <div className="is-size-5 area-items">
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faComments} />
                    <br />
                    Fast Feedback
                  </div>
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faMagnifyingGlassDollar} />
                    <br />
                    Value Stream Curation
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="column is-6">
            <div className="has-background-primary area-of-expertise">
              <p className="title has-text-white area-title">Testing</p>
              <div
                className="content has-text-centered"
                style={{
                  width: "100%",
                  height: "100%",
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <div className="is-size-5 area-items">
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faArrowLeft} />
                    <br />
                    Shift Left
                  </div>
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faPuzzlePiece} />
                    <br /> End-to-End
                  </div>
                </div>
                <div className="is-size-5 area-items">
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faCube} />
                    <br />
                    Unit / Integration
                  </div>
                  <div className="area-item" style={{ width: "50%" }}>
                    <FontAwesomeIcon icon={faForwardFast} />
                    <br />
                    Performance
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
