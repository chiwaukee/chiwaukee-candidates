import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useRef, useState } from "react";
import classnames from "classnames";
import { Link } from "react-router-dom";

function useOutsideAlerter(ref: any, onOutsideClick: any) {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event: any) {
      if (ref.current && !ref.current.contains(event.target)) {
        onOutsideClick();
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
}

export function Menu() {
  const ref = useRef(null);
  const [shouldShowMenu, setShouldShowMenu] = useState(null);

  useOutsideAlerter(ref, () => setShouldShowMenu(false));

  return (
    <div ref={ref} style={{ position: "fixed", top: 0, right: 0, zIndex: 999 }}>
      <div
        className="menu-icon"
        style={{
          fontSize: "1.25rem",
          display: "flex",
          justifyContent: "flex-end",
          width: "100%",
        }}
        role="menu"
        onMouseEnter={() => setShouldShowMenu(true)}
        onClick={() => setShouldShowMenu(!shouldShowMenu)}
      >
        <FontAwesomeIcon icon={faBars} />
      </div>
      <aside
        style={{
          borderRadius: "4px",
          padding: "1rem",
          zIndex: 9999,
          position: "absolute",
          top: 0,
          right: 0,
        }}
        className={classnames("menu has-background-info has-text-right", {
          "slide-in": shouldShowMenu === true,
          "slide-out": shouldShowMenu === false,
          "menu-wrapper": shouldShowMenu === null,
        })}
      >
        <ul className="menu-list">
          <li>
            <Link className="has-text-white" to="/">
              Clients
            </Link>
          </li>
          <li>
            <Link className="has-text-white" to="/candidates">
              Candidates
            </Link>
          </li>
          <li>
            <Link className="has-text-white" to="/about">
              About Us
            </Link>
          </li>
          <li>
            <a
              className="has-text-white"
              href="https://gitlab.com/chiwaukee/chiwaukee-website"
              rel="nopenner noreferrer"
              target="_blank"
            >
              Website Code
            </a>
          </li>
        </ul>
      </aside>
    </div>
  );
}
