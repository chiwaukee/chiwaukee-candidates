import { Officer } from "./Officer";

import timImage from "../../images/tim.png";
import reidImage from "../../images/reid.png";

export function Officers() {
  return (
    <section className="section" id="officers">
      <div className="container has-text-centered">
        <h2 className="title is-2 has-text-left">Who We Are</h2>

        <div className="columns is-multiline is-centered">
          <div className="column is-6">
            <Officer
              email="yahweh@chiwaukee.dev"
              name="Reid Wixom"
              imageSrc={reidImage}
              linkedInUrl="https://www.linkedin.com/in/reid-wixom/"
              title="Chief Executive Officer & Engineering Lead"
              summary="With over 5 years of proven mentorship success, I upskill individuals who may not come from a traditional technology background with ease. I'm currently employed as a Software Engineering Manager at Northwestern Mutual and based out of Milwaukee."
            />
          </div>
          <div className="column is-6">
            <Officer
              email="sensei@chiwaukee.dev"
              name="Timothy Woods"
              imageSrc={timImage}
              linkedInUrl="https://www.linkedin.com/in/timbwoods/"
              title="Chief Operating Officer"
              summary="As the first alum of the program, I have an intimate understanding of the struggles and pitfalls that new candidates may encounter.  I'm currently working as a Support Engineer at Northwestern Mutual and based out of Milwaukee."
            />
          </div>
        </div>
      </div>
    </section>
  );
}
