import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { faCopy, faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { toast } from "bulma-toast";

export function Officer({
  name,
  imageSrc,
  linkedInUrl,
  title,
  summary,
  email,
}: {
  name: string;
  imageSrc: string;
  linkedInUrl?: string;
  title: string;
  summary: string;
  email: string;
}) {
  return (
    <div
      className="card"
      style={{ margin: "0.5em", padding: "0.5rem", height: "100%" }}
    >
      <div
        className="card-image is-1x1"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          paddingTop: "1rem",
        }}
      >
        <figure
          className="image has-background-primary is-1x1 officer-image"
          style={{
            borderRadius: "50%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "350px",
            height: "350px",
          }}
        >
          <img
            src={imageSrc}
            alt={`image of ${name}`}
            className=""
            style={{ maxWidth: "300px" }}
          />
        </figure>
      </div>
      <div className="card-content">
        <div className="media">
          <div className="media-content has-text-left">
            <h4 className="title is-4">
              {name}&nbsp;
              <span className="icon" title="LinkedIn">
                <a href={linkedInUrl} target="_blank" rel="nopenner noreferrer">
                  <FontAwesomeIcon icon={faLinkedin} />
                </a>
              </span>
            </h4>
            <p className="subtitle is-6">{title}</p>
            <p className="has-text-left">{summary}</p>
            <br />
            <p>
              <span>{email}</span>
              <br />
              <a
                title="Open your email client"
                href="mailto:general@chiwaukee.dev"
                className="has-color-primary"
                style={{ display: "inline", height: "100%", cursor: "pointer" }}
              >
                <FontAwesomeIcon icon={faEnvelope} />
              </a>
              &nbsp;&nbsp;
              <span
                title="Copy our email to your clipboard"
                className="has-color-primary"
                style={{ display: "inline", height: "100%", cursor: "pointer" }}
                onClick={() => {
                  navigator.clipboard.writeText(email);
                  toast({
                    duration: 3000,
                    message: "email address copied to clipboard",
                    type: "is-success",
                    dismissible: true,
                  });
                }}
              >
                <FontAwesomeIcon icon={faCopy} />
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
