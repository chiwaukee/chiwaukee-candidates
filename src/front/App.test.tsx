import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { App } from "./App";

xdescribe("The Chiwaukee Website", () => {
  it("should show the title", () => {
    render(<App />);

    expect(screen.getByText(/Client Experience/)).toBeVisible();
  });

  it("should go to the candidate experience page", async () => {
    render(<App />);

    userEvent.click(screen.getByRole("menu"));
    userEvent.click(await screen.findByText("Candidates"));

    expect(await screen.findByText(/Candidate Experience/)).toBeVisible();
  });
});
