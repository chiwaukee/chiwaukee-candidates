import { Areas } from "../Components/Areas";
import { Copyright } from "../Components/Copyright";
import { Hero } from "../Components/Hero";
import { Officers } from "../Components/Officers";

export function Candidates() {
  return (
    <>
      <Hero
        email="candidates@chiwaukee.dev"
        title={
          <>
            Chiwaukee
            <br />
            Developer Program
          </>
        }
        mission="We teach the skills and techniques
                required to stand out in a competitive job market at a cost that undercuts traditional coding bootcamps."
      />
      <Officers />
      <Areas
        title="Tools &amp; Techniques You'll Utilize"
        subtitle="Finish with a full stack starter
                  website that demonstrates skills that employers want to see in
                  candidates."
      />
      <Copyright />
    </>
  );
}
