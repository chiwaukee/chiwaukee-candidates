import { Copyright } from "../Components/Copyright";
import { Hero } from "../Components/Hero";
import { Officers } from "../Components/Officers";

export function Mission() {
  return (
    <>
      <Hero
        email="clients@chiwaukee.dev"
        title={<>Chiwaukee</>}
        mission="We provide digital solutions by utilizing individuals
                from under-represented groups in tech at rates that bridge
                equity and talent gaps, with an emphasis on technical
                excellence."
      />
      <Officers />
      <Copyright />
    </>
  );
}
