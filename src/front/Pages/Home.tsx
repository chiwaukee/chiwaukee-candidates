import { Areas } from "../Components/Areas";
import { Copyright } from "../Components/Copyright";
import { Hero } from "../Components/Hero";
import { Officers } from "../Components/Officers";
import { Ways } from "../Components/Ways";

export function Home() {
  return (
    <>
      <Hero
        email="clients@chiwaukee.dev"
        title={
          <>
            Chiwaukee
            <br />
            Consulting
          </>
        }
        mission="We provide technical guidance and digital solutions with flexible terms that exceed client expectations and objectives."
      />
      <Officers />
      <Ways />
      <Areas title="How We Do It" />
      <Copyright />
    </>
  );
}
