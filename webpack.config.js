const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const config = require("config");

const isDev = config.get("shouldUseDevelopmentFeatures");

const devtool = isDev ? { devtool: "source-map" } : null;

console.log(isDev ? "development" : "production");

module.exports = {
  ...devtool,
  mode: isDev ? "development" : "production",
  entry: "./src/front/index",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader",
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },

      {
        test: /\.md$/i,
        use: "raw-loader",
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist", "front"),
    publicPath: "/",
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/front/index.ejs",
      favicon: "./src/front/images/favicon.ico",
      templateParameters: {
        prod: !isDev,
      },
    }),
  ],
};
