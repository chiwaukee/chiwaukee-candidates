## [0.48.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.47.0...v0.48.0) (2022-09-05)


### Features

* update menu items ([d397e0c](https://gitlab.com/chiwaukee/chiwaukee-website/commit/d397e0c3c369dca0bb69f65a047c1b3c85233a62))


### Bug Fixes

* rename to candidates ([9bc37cf](https://gitlab.com/chiwaukee/chiwaukee-website/commit/9bc37cf664ff11e696ba834cbd7c6ea54a7f5aa6))

## [0.47.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.46.2...v0.47.0) (2022-09-04)


### Features

* update wording on CanEx ([20c7169](https://gitlab.com/chiwaukee/chiwaukee-website/commit/20c7169f814553ddbe275246a3fa118042be373e))

## [0.46.2](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.46.1...v0.46.2) (2022-09-04)


### Bug Fixes

* fix supertest config ([ca655b8](https://gitlab.com/chiwaukee/chiwaukee-website/commit/ca655b8baef8167c713169657732c4dd923a6ac6))
* fix supertest config ([e9ce71b](https://gitlab.com/chiwaukee/chiwaukee-website/commit/e9ce71b5ce9cd4a3ab8d91241799edccdbad2cd8))
* jest collect coverage from all files ([12b5353](https://gitlab.com/chiwaukee/chiwaukee-website/commit/12b5353a093ff29ff8448e7030572c931f3d67b8))
* jest collect coverage from all files ([0d0c594](https://gitlab.com/chiwaukee/chiwaukee-website/commit/0d0c594919b501b8f6e77570355581eca93a10e3))
* prune unused dependencies ([8aa536b](https://gitlab.com/chiwaukee/chiwaukee-website/commit/8aa536b165562c4118f00758e7f822da76f8c193))
* prune unused dependencies ([af91814](https://gitlab.com/chiwaukee/chiwaukee-website/commit/af91814f5ca7ff548ee312351e56eac56a70c8dc))
* prune unused dependencies ([0c4411d](https://gitlab.com/chiwaukee/chiwaukee-website/commit/0c4411d2cbee577033647dccfcd222ec62b9644d))
* prune unused dependencies ([e237ff8](https://gitlab.com/chiwaukee/chiwaukee-website/commit/e237ff803348106d7d836636a44d6fe9968042fb))
* revert some changes ([704ee97](https://gitlab.com/chiwaukee/chiwaukee-website/commit/704ee97c7f1a946fb8889f2de8b616c04f64595f))
* update jest config and add to back ([612d16b](https://gitlab.com/chiwaukee/chiwaukee-website/commit/612d16bc529b2145a5a8dec3fe356c347ca16c04))
* update sonar properties ([e9de3ad](https://gitlab.com/chiwaukee/chiwaukee-website/commit/e9de3ad6aa25f7b27a25167bdbd3aa102814a46f))

## [0.46.1](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.46.0...v0.46.1) (2022-09-04)


### Bug Fixes

* update sonar properties ([10a011d](https://gitlab.com/chiwaukee/chiwaukee-website/commit/10a011d839c753fd48c593afc013e070676b9b29))
* update sonar properties ([2d99313](https://gitlab.com/chiwaukee/chiwaukee-website/commit/2d993137fb457acbb4c4882a0a4c69ef1bd75d14))
* update sonar properties ([30bf4e0](https://gitlab.com/chiwaukee/chiwaukee-website/commit/30bf4e07eb2177cfe809fe48df304fb39a4d28c9))

## [0.46.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.45.0...v0.46.0) (2022-09-04)


### Features

* add jest coverage ([637c183](https://gitlab.com/chiwaukee/chiwaukee-website/commit/637c183fbe02a1950b86086fcae1aa3076788200))
* add jest for testing ([9589794](https://gitlab.com/chiwaukee/chiwaukee-website/commit/9589794db744ba6ae9b80bc4c35ea24fd14c9cc3))
* add test to pipeline ([84360fa](https://gitlab.com/chiwaukee/chiwaukee-website/commit/84360fa3bfa3ef78a12cd39dc3a3134df88dc3b0))


### Bug Fixes

* include test artifacts ([da177d7](https://gitlab.com/chiwaukee/chiwaukee-website/commit/da177d73fe6fdb917b2eecd8ec52a5903f20458f))
* include test artifacts ([09d182f](https://gitlab.com/chiwaukee/chiwaukee-website/commit/09d182fb5e2d45154204b9558a0180eef0235083))
* require tests before running sonar ([2e5b07d](https://gitlab.com/chiwaukee/chiwaukee-website/commit/2e5b07dda1994bb38be21602731e3e4023319cd3))

## [0.45.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.44.0...v0.45.0) (2022-09-04)


### Features

* update CanEx mission ([b900951](https://gitlab.com/chiwaukee/chiwaukee-website/commit/b90095139ab455e6f3ea6383aff58dbefa0b763d))

## [0.44.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.43.0...v0.44.0) (2022-09-04)


### Features

* refactor and add candidate experience page ([e5a7a96](https://gitlab.com/chiwaukee/chiwaukee-website/commit/e5a7a9661b372f219d57c0dbfc1cae9eeb12f520))
* remove blog code and update mission ([97f0e45](https://gitlab.com/chiwaukee/chiwaukee-website/commit/97f0e451b811c15345bafb3d99d6dc21c4bed0eb))

## [0.43.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.42.0...v0.43.0) (2022-09-03)


### Features

* update pipeline ([e31ae9a](https://gitlab.com/chiwaukee/chiwaukee-website/commit/e31ae9ad525a9c76e7a8d10c0d530134a34fd602))

## [0.42.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.41.0...v0.42.0) (2022-09-03)


### Features

* change menu options ([dbf17db](https://gitlab.com/chiwaukee/chiwaukee-website/commit/dbf17dbfd4d5be7de9339ddda65bf3742f6bf236))

## [0.41.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.40.1...v0.41.0) (2022-09-03)


### Features

* add updated eslint-config that included prettier ([46fb7e0](https://gitlab.com/chiwaukee/chiwaukee-website/commit/46fb7e0b62b8be96881092df41d9fc3083a912eb))

## [0.40.1](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.40.0...v0.40.1) (2022-09-03)


### Bug Fixes

* fix logo on mobile ([745da57](https://gitlab.com/chiwaukee/chiwaukee-website/commit/745da571dfab882902d01e3574e20a7395a460ae))

## [0.40.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.6...v0.40.0) (2022-08-28)


### Features

* re-add blog ([3267b2a](https://gitlab.com/chiwaukee/chiwaukee-website/commit/3267b2af4f3b6f29ab02883fe2096da573a213b0))

## [0.39.6](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.5...v0.39.6) (2022-08-28)


### Bug Fixes

* lint ([4f3cfbf](https://gitlab.com/chiwaukee/chiwaukee-website/commit/4f3cfbf35caa651e6b2e61b8fdfe42d4eb4f133c))

## [0.39.5](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.4...v0.39.5) (2022-08-28)


### Bug Fixes

* missing lint ([87e79be](https://gitlab.com/chiwaukee/chiwaukee-website/commit/87e79be5b9e3fb6ca55678da260087281e82328c))

## [0.39.4](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.3...v0.39.4) (2022-08-28)


### Bug Fixes

* logo on desktop ([65457f4](https://gitlab.com/chiwaukee/chiwaukee-website/commit/65457f42d5234178235560212b1ff91977a78abe))

## [0.39.3](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.2...v0.39.3) (2022-08-28)


### Bug Fixes

* color of logo in dark mode ([b0aa088](https://gitlab.com/chiwaukee/chiwaukee-website/commit/b0aa088acda33100b6746edbcc82298b3771d49d))

## [0.39.2](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.1...v0.39.2) (2022-08-28)


### Bug Fixes

* enable analytics ([c4e727b](https://gitlab.com/chiwaukee/chiwaukee-website/commit/c4e727b4ce012cf1bbfc1fb0960985915a52e3d7))

## [0.39.1](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.39.0...v0.39.1) (2022-08-28)


### Bug Fixes

* enable analytics ([cdeb13b](https://gitlab.com/chiwaukee/chiwaukee-website/commit/cdeb13b42dc29a77e40717421cc552ae8f5c2cb2))

## [0.39.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.38.0...v0.39.0) (2022-08-28)


### Features

* reduce bundle size ([7140478](https://gitlab.com/chiwaukee/chiwaukee-website/commit/7140478dcfb6d2684700a829aac3d7c31ba97916))
* reduce bundle size ([e6ce663](https://gitlab.com/chiwaukee/chiwaukee-website/commit/e6ce6636e59f21d2e7856c2e792b1beffb1e17eb))

## [0.38.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.37.0...v0.38.0) (2022-08-27)


### Features

* google analytics ([6479b12](https://gitlab.com/chiwaukee/chiwaukee-website/commit/6479b129b303d276d2f30d11a9f8c0df805efc82))

## [0.37.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.36.0...v0.37.0) (2022-08-27)


### Features

* google analytics ([2d2d815](https://gitlab.com/chiwaukee/chiwaukee-website/commit/2d2d81541f1f03375290e18f2a17ebc9c028a302))

## [0.36.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.35.1...v0.36.0) (2022-08-27)


### Features

* google analytics ([21c6b8f](https://gitlab.com/chiwaukee/chiwaukee-website/commit/21c6b8f663a02866dec331d9ef70e9050e07c72d))
* google analytics ([1a7bf72](https://gitlab.com/chiwaukee/chiwaukee-website/commit/1a7bf72bab02206157ca194d543b557af7f582a7))

## [0.35.1](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.35.0...v0.35.1) (2022-08-27)


### Bug Fixes

* remove sentry ([ef096bd](https://gitlab.com/chiwaukee/chiwaukee-website/commit/ef096bdd09966bc5dc18a3ceecb9e0e12b43cda4))

## [0.35.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.34.0...v0.35.0) (2022-08-27)


### Features

* add analytics ([2560b60](https://gitlab.com/chiwaukee/chiwaukee-website/commit/2560b6057f37c5beb85ad08cd9ec2e1a54222121))

## [0.34.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.33.0...v0.34.0) (2022-08-27)


### Features

* add analytics ([a576559](https://gitlab.com/chiwaukee/chiwaukee-website/commit/a576559a5c8e1467cf0c3f625afc17f93be64006))

## [0.33.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.32.0...v0.33.0) (2022-08-27)


### Features

* add helmet and sentry ([8d0b441](https://gitlab.com/chiwaukee/chiwaukee-website/commit/8d0b4412c74e814ff6c7bf2448f741dc58b8db29))
* add helmet and sentry ([28f1ac4](https://gitlab.com/chiwaukee/chiwaukee-website/commit/28f1ac42b8bd78e414871a745490546b4fa02346))
* add sentry server-side ([791ca27](https://gitlab.com/chiwaukee/chiwaukee-website/commit/791ca279da2986e3c2f97bfcf314b9684cb826a1))


### Bug Fixes

* decrease trace sample rate ([fdde206](https://gitlab.com/chiwaukee/chiwaukee-website/commit/fdde206197911f022c2c41fe0eb182649889de06))

## [0.32.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.31.1...v0.32.0) (2022-08-27)


### Features

* ddd CSRF protection ([0c84318](https://gitlab.com/chiwaukee/chiwaukee-website/commit/0c84318f226849a97fd0b5823288fee8efd295f3))

## [0.31.1](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.31.0...v0.31.1) (2022-08-27)


### Bug Fixes

* disable express header ([28bfdb3](https://gitlab.com/chiwaukee/chiwaukee-website/commit/28bfdb3f5a46b0c6af2b79c1a8e1b967b298d83e))

# [0.31.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.30.0...v0.31.0) (2022-08-27)


### Features

* gitlab release step ([b941dbf](https://gitlab.com/chiwaukee/chiwaukee-website/commit/b941dbf3908e443be6202aa3c2871fb955670818))

# [0.30.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.29.0...v0.30.0) (2022-08-27)


### Features

* add git blogpost ([b8911fc](https://gitlab.com/chiwaukee/chiwaukee-website/commit/b8911fc17131b76b72a755f110813932f77336dc))

# [0.29.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.28.0...v0.29.0) (2022-08-26)


### Features

* fix npm config ([f3fd200](https://gitlab.com/chiwaukee/chiwaukee-website/commit/f3fd2006bf7897c4bc03595522bdd23d118a25b3))

# [0.28.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.27.0...v0.28.0) (2022-08-26)


### Features

* consolidate extraneous configs ([288ca46](https://gitlab.com/chiwaukee/chiwaukee-website/commit/288ca46b6c2c2104fb3fd75fc7aa6a4dea555f2d))
* use semantic-release-config ([ebd5fc7](https://gitlab.com/chiwaukee/chiwaukee-website/commit/ebd5fc723c71e49fc2bb6e4068372c3768129935))

# [0.27.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.26.0...v0.27.0) (2022-08-26)


### Features

* fix sonar ([7f866ab](https://gitlab.com/chiwaukee/chiwaukee-website/commit/7f866ab1ffd75e5f75eb646e646347df110cdf20))
* fix sonar ([487b6b1](https://gitlab.com/chiwaukee/chiwaukee-website/commit/487b6b11d13b31b255fb4061095d7e12a40383bb))

# [0.26.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.25.0...v0.26.0) (2022-08-26)


### Features

* resolve sonar hotspot ([c539d1f](https://gitlab.com/chiwaukee/chiwaukee-website/commit/c539d1f530d1ea47fb3e8d7904e18c0c89bba268))

# [0.25.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.24.1...v0.25.0) (2022-08-26)


### Features

* add sonarcloud ([aa5ef53](https://gitlab.com/chiwaukee/chiwaukee-website/commit/aa5ef537781180f5929773f0220080d5f6095088))
* add sonarcloud ([4173019](https://gitlab.com/chiwaukee/chiwaukee-website/commit/4173019c71978fa4e5f77d788292c77000cc010d))

## [0.24.1](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.24.0...v0.24.1) (2022-08-26)


### Bug Fixes

* patch svg vulnerabilities ([1c19554](https://gitlab.com/chiwaukee/chiwaukee-website/commit/1c195545369ca6d1bc61840531aae6178d64f802))

# [0.24.0](https://gitlab.com/chiwaukee/chiwaukee-website/compare/v0.23.0...v0.24.0) (2022-08-26)


### Features

* change font weight of general email ([3a17f34](https://gitlab.com/chiwaukee/chiwaukee-website/commit/3a17f346a09f8c24cd7d27389f6f204209a816b7))

# [0.23.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.22.0...v0.23.0) (2022-08-26)


### Features

* enable webkit menu animation ([bfbf87b](https://gitlab.com/lameimpala/chiwaukee-website/commit/bfbf87b06d3a44e7f662a38674dee82f5b54f26c))

# [0.22.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.21.0...v0.22.0) (2022-08-26)


### Features

* change colors ([c9e6c97](https://gitlab.com/lameimpala/chiwaukee-website/commit/c9e6c97f4adbb902f3a54d9c52ae73a1ea13ea49))

# [0.21.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.20.0...v0.21.0) (2022-08-26)


### Features

* add blog and menu and links ([8abf6e7](https://gitlab.com/lameimpala/chiwaukee-website/commit/8abf6e7ac6f21edc19fd038f7bce5b379536ae6d))

# [0.20.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.19.0...v0.20.0) (2022-08-26)


### Features

* soften logo fill ([fa7c735](https://gitlab.com/lameimpala/chiwaukee-website/commit/fa7c735a0029ce95ad15d18093804fbef4bf55c4))

# [0.19.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.18.0...v0.19.0) (2022-08-26)


### Features

* update style and add ways section ([4fe2a0c](https://gitlab.com/lameimpala/chiwaukee-website/commit/4fe2a0c620041e479978ce3961a7e0f56b5ec339))

# [0.18.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.17.0...v0.18.0) (2022-08-26)


### Features

* add ways we can help ([4e37b1e](https://gitlab.com/lameimpala/chiwaukee-website/commit/4e37b1eb924c9ed4177e5a8fd217c72cf4c2685e))
* update names ([14e3ea7](https://gitlab.com/lameimpala/chiwaukee-website/commit/14e3ea7c93fdbdaf31d7c4bff56128f606e4b305))

# [0.17.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.16.2...v0.17.0) (2022-08-26)


### Bug Fixes

* remove old file ([13edbf5](https://gitlab.com/lameimpala/chiwaukee-website/commit/13edbf5df738313945d73a9b43e9207e196783cf))


### Features

* update layout and svg ([6013b9f](https://gitlab.com/lameimpala/chiwaukee-website/commit/6013b9f012de8d376f40ec89e20aeb3ed85bec80))

## [0.16.2](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.16.1...v0.16.2) (2022-08-25)


### Bug Fixes

* logo and color ([8af9c22](https://gitlab.com/lameimpala/chiwaukee-website/commit/8af9c22c92dea65970e531b5c64f93cad9bde11c))
* logo and color ([06e120a](https://gitlab.com/lameimpala/chiwaukee-website/commit/06e120a6ad173b5e115a3ec477d39a30e85e7277))

## [0.16.1](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.16.0...v0.16.1) (2022-08-25)


### Bug Fixes

* increase width of area items ([b3e81bd](https://gitlab.com/lameimpala/chiwaukee-website/commit/b3e81bd105acae031ddad1664fd1c0f71733bd6d))
* text color on areas ([659aeec](https://gitlab.com/lameimpala/chiwaukee-website/commit/659aeecabdfb41d99112d232c14e1ad61d5e374e))

# [0.16.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.15.1...v0.16.0) (2022-08-25)


### Features

* add officer emails ([7b67ec7](https://gitlab.com/lameimpala/chiwaukee-website/commit/7b67ec72ad45533919dd5450d732a5ac475eeeea))

## [0.15.1](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.15.0...v0.15.1) (2022-08-25)


### Bug Fixes

* set logo fill correctly when in light mode ([0644b8a](https://gitlab.com/lameimpala/chiwaukee-website/commit/0644b8a72cd82b0634421fbc97a98570e2b2d336))

# [0.15.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.14.0...v0.15.0) (2022-08-25)


### Features

* change layout ([6bb0aa3](https://gitlab.com/lameimpala/chiwaukee-website/commit/6bb0aa350af64a441c6eb603553c433ee2566626))
* change layout ([8721ccc](https://gitlab.com/lameimpala/chiwaukee-website/commit/8721ccc05d4d78d558a9dae7a19d919dcd115f32))

# [0.14.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.13.0...v0.14.0) (2022-08-25)


### Bug Fixes

* add spaces ([e3634ab](https://gitlab.com/lameimpala/chiwaukee-website/commit/e3634abde3b87bd6724a4f8580d9f55591f9b918))


### Features

* refactor and improve styles ([6017915](https://gitlab.com/lameimpala/chiwaukee-website/commit/6017915c5612caa8a401f757200ab8c9ffc93ee1))

# [0.13.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.12.0...v0.13.0) (2022-08-25)


### Features

* add icons and content to areas ([8db8be8](https://gitlab.com/lameimpala/chiwaukee-website/commit/8db8be8b093247e63f397ef5c77ac64e73ce15dc))

# [0.12.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.11.0...v0.12.0) (2022-08-22)


### Features

* add areas section ([8f8d300](https://gitlab.com/lameimpala/chiwaukee-website/commit/8f8d300a03fa9c7cf1887edea55c0a3a82be10a3))

# [0.11.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.10.0...v0.11.0) (2022-08-21)


### Features

* update icon and logo ([3643964](https://gitlab.com/lameimpala/chiwaukee-website/commit/36439643f47f62847dc4aeffaffb7cd1aef5a8e0))

# [0.10.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.9.4...v0.10.0) (2022-08-21)


### Features

* update icons and refactor ([6585415](https://gitlab.com/lameimpala/chiwaukee-website/commit/65854157ce7b90b5be10212c2b062c026199841c))

## [0.9.4](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.9.3...v0.9.4) (2022-08-21)


### Bug Fixes

* responsive styling ([4e68077](https://gitlab.com/lameimpala/chiwaukee-website/commit/4e680772cc10b82f50b3d81bdf65e9ca28cc17f5))

## [0.9.3](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.9.2...v0.9.3) (2022-08-21)


### Bug Fixes

* favicon on iphone ([2db5271](https://gitlab.com/lameimpala/chiwaukee-website/commit/2db52712d68d46d78bae16146c9ef418e3f0c578))

## [0.9.2](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.9.1...v0.9.2) (2022-08-21)


### Bug Fixes

* responsive styling ([c00fcb7](https://gitlab.com/lameimpala/chiwaukee-website/commit/c00fcb779f6a1f34202634ba544d8cb5ea947ccd))

## [0.9.1](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.9.0...v0.9.1) (2022-08-21)


### Bug Fixes

* set height on banner correctly ([04610c3](https://gitlab.com/lameimpala/chiwaukee-website/commit/04610c3807fae0a630495c008bee05d68c47b89b))

# [0.9.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.8.0...v0.9.0) (2022-08-21)


### Features

* change colors ([1f89631](https://gitlab.com/lameimpala/chiwaukee-website/commit/1f89631b731d764b70553b113b51aed06ce3717d))

# [0.8.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.7.2...v0.8.0) (2022-08-21)


### Features

* add svg logo ([e1a90fb](https://gitlab.com/lameimpala/chiwaukee-website/commit/e1a90fb43cba05c6be3a9c28312a30e5eff98343))

## [0.7.2](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.7.1...v0.7.2) (2022-08-21)


### Bug Fixes

* height ([9dee1e0](https://gitlab.com/lameimpala/chiwaukee-website/commit/9dee1e0afbb996fc64e7241b86cd9f5be596adbb))

## [0.7.1](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.7.0...v0.7.1) (2022-08-21)


### Bug Fixes

* set width on logo ([f72c4e2](https://gitlab.com/lameimpala/chiwaukee-website/commit/f72c4e21ebb72bcf839d24075a4fe26de3d55d99))

# [0.7.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.6.0...v0.7.0) (2022-08-21)


### Features

* update favicon ([7be6270](https://gitlab.com/lameimpala/chiwaukee-website/commit/7be6270b31746d6994e4b1e654d25b5d385e3a88))

# [0.6.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.5.0...v0.6.0) (2022-08-21)


### Features

* update logo ([7584292](https://gitlab.com/lameimpala/chiwaukee-website/commit/7584292042510f6b39d68616e08f1e17f050f680))

# [0.5.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.4.0...v0.5.0) (2022-08-21)


### Features

* improve responsive design ([706bc30](https://gitlab.com/lameimpala/chiwaukee-website/commit/706bc3023a9260c62bbc23bba3414bafc24974cf))

# [0.4.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.3.0...v0.4.0) (2022-08-21)


### Features

* update logo ([da8ba95](https://gitlab.com/lameimpala/chiwaukee-website/commit/da8ba959ae9eff23654718292932e4b4138d43f2))

# [0.3.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.2.0...v0.3.0) (2022-08-21)


### Features

* major refactor ([a6f2885](https://gitlab.com/lameimpala/chiwaukee-website/commit/a6f2885b7c0cf6063541db091c7f313ccf2a8a66))

# [0.2.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.1.1...v0.2.0) (2022-08-21)


### Features

* major refactor ([342dbb2](https://gitlab.com/lameimpala/chiwaukee-website/commit/342dbb2d83e94dcae28968c247152c34d13d8bda))

# [0.1.0](https://gitlab.com/lameimpala/chiwaukee-website/compare/v0.0.15...v0.1.0) (2022-08-20)


### Bug Fixes

* change template references ([8515f20](https://gitlab.com/lameimpala/chiwaukee-website/commit/8515f20b2663c6cfdacc065a57f304872ecc2680))
* set package.json to private ([1087a76](https://gitlab.com/lameimpala/chiwaukee-website/commit/1087a769b5b16385e99ee5aa9813b268f148404f))


### Features

* add commitlint ([3d93dd4](https://gitlab.com/lameimpala/chiwaukee-website/commit/3d93dd4a95be0788a670a3de780ecc61aa899eb2))
* add semantic release ([34709d9](https://gitlab.com/lameimpala/chiwaukee-website/commit/34709d95ffa7691a64b93e17e9fd34f9c891783e))
